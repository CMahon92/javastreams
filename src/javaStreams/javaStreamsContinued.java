package javaStreams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.testng.Assert;

public class javaStreamsContinued {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		List <String> otherNames = Arrays.asList("Olivia","Leena", "Jessie", "Marshall", "Tom", "Lala");
		List <String> names = Arrays.asList("Aaron","John", "Adam","Amy","Bill");
		Stream <String> newStream = Stream.concat(otherNames.stream(), names.stream());

		//How to confirm a string is present within a list
		boolean flag = newStream.anyMatch(s->s.equalsIgnoreCase("Olivia"));
		System.out.println(flag);
		Assert.assertTrue(flag);
	
	
		//creating a new list from current list
		List <String> ls = Stream.of("amanda","lori","sean","luna").filter(s->s.endsWith("a")).map(s->s.toUpperCase())
		.collect(Collectors.toList());
		System.out.println(ls.get(1));
	
		//print unique numbers from this list
		//sort the array
		//get 3rd index
		
		List <Integer> values = Arrays.asList(3,2,2,7,5,1,9,7);
		List <Integer> li = values.stream().distinct().sorted().collect(Collectors.toList());
		System.out.println(li); 
		//System.out.println(li.get(2)); Prints 3
		
	}

}
