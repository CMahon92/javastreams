package javaStreams;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class webTableSorting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/offers");
		
		driver.findElement(By.xpath("//tr/th[1]")).click();
		
		//Captures WebElements to List
		List <WebElement> elementList = driver.findElements(By.xpath("//tr/td[1]"));
		
		//Captures text of all web elements into new (original list)
		List <String> orginalList = elementList.stream().map(s->s.getText()).collect(Collectors.toList());
		
		//sort the "originalList" into a sorted list 
		List <String> sortedList = orginalList.stream().sorted().collect(Collectors.toList());
		
		//Compare ooriginalList to sortedList
		Assert.assertTrue(orginalList.equals(sortedList));
	}

}
