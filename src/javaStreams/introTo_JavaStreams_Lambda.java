package javaStreams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.testng.Assert;

public class introTo_JavaStreams_Lambda {
	
	public static void main(String[] args) {


// 	First way of getting the count of names starting with the letter "A"
//	ArrayList<String> names = new ArrayList<String>();
//	names.add("Aaron");
//	names.add("John");
//	names.add("Adam");
//	names.add("Amy");
//	names.add("Bill");
//	int count = 0;
//	
//	for (int i=0;i<names.size();i++) 
//	{
//		String actual=names.get(i);
//		if(actual.startsWith("A")) 
//		{
//			count++;
//		}
//	}
//	
//	System.out.println(count);
	
//	How to solve the same problem above using JavaStream and Lambda
		ArrayList<String> names = new ArrayList<String>();
		names.add("Aaron");
		names.add("John");
		names.add("Adam");
		names.add("Amy");
		names.add("Bill");
//		
//		Long c = names.stream().filter(s->s.startsWith("A")).count();
//		System.out.println(c);
		
		
//		How to solve the same problem by using the names within the stream
		Long d = Stream.of("Aaron","John", "Adam","Amy","Bill").filter(s->s.startsWith("A")).count();
		System.out.println("Total number of names that begin with the letter 'A' is " + d);
		
//		How to get the names of an Arraylist w/ a length greater than 3
		names.stream().filter(y->y.length()>3).forEach(y->System.out.println("This name has a length greater than 3: "+ y));

//		Print only the first result
		names.stream().filter(y->y.length()>3).limit(1).forEach(y->System.out.println("Result for limit: " + y));
		
//		How to modify an ArrayList using map
		Stream.of("Aaron","John", "Adam","Amy","Bill").filter(s->s.endsWith("n")).map(s->s.toUpperCase())
		.forEach(s->System.out.println("This name ends with the letter 'n' and has been capitalized : " + s));
		
//		List of names sorted
		List <String> otherNames = Arrays.asList("Olivia","Leena", "Jessie", "Marshall", "Tom", "Lala");
		otherNames.stream().filter(s->s.endsWith("a")).sorted().forEach(s->System.out.println("Sorted name that ends with 'a': "+s));
		
//		Merge two strings
		Stream <String> newStream = Stream.concat(otherNames.stream(), names.stream());
		newStream.sorted().forEach(s->System.out.println(s));
		

	}

}
	
